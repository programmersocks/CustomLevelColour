plugins {
    java
    id("fabric-loom") version "1.6-SNAPSHOT"
}

val minecraftVersion = "1.21"
version = "1.0.0+$minecraftVersion"
group = "net.inyourwalls"

repositories {
    maven("https://maven.terraformersmc.com/")
}

dependencies {
    val yarnBuild = "1"
    val fabricLoaderVersion = "0.14.22"

    minecraft("com.mojang:minecraft:$minecraftVersion")
    mappings("net.fabricmc:yarn:$minecraftVersion+build.$yarnBuild:v2")
    modImplementation("net.fabricmc:fabric-loader:$fabricLoaderVersion")
    modImplementation("com.terraformersmc:modmenu:9.0.0")
}

tasks.processResources {
    inputs.property("version", project.version)
    filesMatching("fabric.mod.json") {
        expand(mapOf(
            "version" to project.version
        ))
    }
}
