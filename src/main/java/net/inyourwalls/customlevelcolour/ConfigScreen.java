package net.inyourwalls.customlevelcolour;

import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.text.Text;

import java.util.regex.Pattern;

import static net.minecraft.client.gui.widget.ButtonWidget.DEFAULT_HEIGHT;
import static net.minecraft.client.gui.widget.ButtonWidget.DEFAULT_WIDTH;
import static net.minecraft.client.gui.widget.ButtonWidget.DEFAULT_WIDTH_SMALL;

public class ConfigScreen extends Screen {
    private static final Text TITLE = Text.translatable("gui.customlevelcolour.title");
    private static final Text OUTLINE_COLOUR_LABEL = Text.translatable("gui.customlevelcolour.outlineColour");
    private static final Text TEXT_COLOUR_LABEL = Text.translatable("gui.customlevelcolour.textColour");
    private static final Pattern HEX_COLOUR_REGEX = Pattern.compile("[0-9a-fA-F]{0,6}");
    private final Screen previous;
    private final CustomLevelColourConfig config;
    private int previewOutlineColour;
    private int previewTextColour;

    public ConfigScreen() {
        this(null);
    }

    public ConfigScreen(Screen previous) {
        super(TITLE);
        this.previous = previous;
        this.config = CustomLevelColour.getConfig();
    }

    @Override
    public void init() {
        TextFieldWidget outlineField = new TextFieldWidget(
            this.textRenderer,
            this.width / 2 + 4,
            25,
            DEFAULT_WIDTH_SMALL,
            DEFAULT_HEIGHT,
            OUTLINE_COLOUR_LABEL
        );
        outlineField.setTextPredicate(HEX_COLOUR_REGEX.asMatchPredicate());
        outlineField.setChangedListener(text -> {
            try {
                this.previewOutlineColour = Integer.parseInt(text, 16);
            } catch (NumberFormatException ex) {
                // fail silently
            }
        }); 
        outlineField.setText(String.format("%x", config.outlineColour));
        TextFieldWidget textField = new TextFieldWidget(
            this.textRenderer,
            this.width / 2 + 4,
            55,
            DEFAULT_WIDTH_SMALL,
            DEFAULT_HEIGHT,
            TEXT_COLOUR_LABEL
        );
        textField.setTextPredicate(HEX_COLOUR_REGEX.asMatchPredicate()); 
        textField.setChangedListener(text -> {
            try {
                this.previewTextColour = Integer.parseInt(text, 16);
            } catch (NumberFormatException ex) {}
        }); 
        textField.setText(String.format("%x", config.textColour));

        ButtonWidget backButton = ButtonWidget.builder(Text.translatable("gui.back"), thisButton -> this.close())
            .size(DEFAULT_WIDTH, DEFAULT_HEIGHT)
            .position(this.width / 2 - DEFAULT_WIDTH - 4, this.height - 28)
            .build();
        ButtonWidget saveButton = ButtonWidget.builder(Text.translatable("selectWorld.edit.save"), thisButton -> {
            try {
                config.outlineColour = Integer.parseInt(outlineField.getText(), 16);
                config.textColour = Integer.parseInt(textField.getText(), 16);
            } catch (NumberFormatException ex) {}
            CustomLevelColour.saveConfig();
        })
        .size(DEFAULT_WIDTH, DEFAULT_HEIGHT)
        .position(this.width / 2 + 4, this.height - 28)
        .build();

        this.addDrawableChild(outlineField);
        this.addDrawableChild(textField);
        this.addDrawableChild(backButton);
        this.addDrawableChild(saveButton);
    }

    @Override
    public void close() {
        this.client.setScreen(this.previous); 
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        super.render(context, mouseX, mouseY, delta);
        context.drawCenteredTextWithShadow(this.textRenderer, TITLE, this.width / 2, 5, 0xffffff);

        int labelX = this.width / 2 - Math.max(
            this.textRenderer.getWidth(TEXT_COLOUR_LABEL),
            this.textRenderer.getWidth(OUTLINE_COLOUR_LABEL)
        ) - 4;
        context.drawTextWithShadow(this.textRenderer, OUTLINE_COLOUR_LABEL, labelX, 30, 0xffffff);
        context.drawTextWithShadow(this.textRenderer, TEXT_COLOUR_LABEL, labelX, 60, 0xffffff);
    }
}
