package net.inyourwalls.customlevelcolour;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

public class CustomLevelColourConfig {
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    public int outlineColour = 0x000000;
    public int textColour = 0x80ff20;

    public static CustomLevelColourConfig load(Path path) throws IOException {
        FileReader reader = new FileReader(path.toFile());
        return GSON.fromJson(reader, CustomLevelColourConfig.class);
    }

    public static void save(CustomLevelColourConfig config, Path path) throws IOException {
        FileWriter writer = new FileWriter(path.toFile());
        GSON.toJson(config, writer);
        writer.close();
    }
}
