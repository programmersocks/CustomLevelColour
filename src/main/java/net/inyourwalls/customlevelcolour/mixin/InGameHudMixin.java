// InGameHudMixin: Changes the colour of the level text & outline.
package net.inyourwalls.customlevelcolour.mixin;

import net.inyourwalls.customlevelcolour.CustomLevelColour;
import net.minecraft.client.gui.hud.InGameHud;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;

@Mixin(InGameHud.class)
public class InGameHudMixin {
    @ModifyArg(
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/gui/DrawContext;drawText(Lnet/minecraft/client/font/TextRenderer;Ljava/lang/String;IIIZ)I"
        ),
        method = "renderExperienceLevel",
        index = 4
    )
    private int overwriteOutlineColour(int oldColour) {
        return CustomLevelColour.getConfig().outlineColour;
    }

    @ModifyArg(
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/gui/DrawContext;drawText(Lnet/minecraft/client/font/TextRenderer;Ljava/lang/String;IIIZ)I",
            ordinal = 4
        ),
        method = "renderExperienceLevel",
        index = 4
    )
    private int overwriteTextColour(int oldColour) {
        return CustomLevelColour.getConfig().textColour;
    }
}
