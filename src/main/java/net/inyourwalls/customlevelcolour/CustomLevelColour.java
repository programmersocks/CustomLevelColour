package net.inyourwalls.customlevelcolour;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;

public class CustomLevelColour implements ClientModInitializer {
    public static final Logger LOGGER = LoggerFactory.getLogger(CustomLevelColour.class);
    private static final Path CONFIG_PATH = FabricLoader.getInstance().getConfigDir().resolve("customlevelcolour.json");
    private static CustomLevelColourConfig config;

    @Override
    public void onInitializeClient() {
        try {
            config = CustomLevelColourConfig.load(CONFIG_PATH);
        } catch (IOException ex) {
            LOGGER.debug("Failed to load config", ex);
            config = new CustomLevelColourConfig();
        }
    }

    public static CustomLevelColourConfig getConfig() {
        if (config == null) throw new IllegalStateException("Attempted to get the configuration before it was loaded");
        return config;
    }

    public static void saveConfig() {
        if (config == null) throw new IllegalStateException("Attempted to save the configuration before it was loaded");
        try {
            CustomLevelColourConfig.save(config, CONFIG_PATH);
        } catch (IOException ex) {
            LOGGER.error("Failed to save configuration file", ex);
        }
    }
}
